

// import {randomUserMock} from "./FE4U-Lab3-mock.js";
// import {additionalUsers} from "./FE4U-Lab3-mock.js";


let users = await getRandomUsers(50);
users = users.concat(await getUsersFromDB());
console.log(validateUsersFromArray(users));
let currentUsers = users;
let gridPageNum = 0;
let numInGallery = 0;
// let statisticsPageNum = 1;
let sortingType = null;
let reverseSort = false;

let map = L.map('leaflet-map').setView([0, 0], 15);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);
let marker = new L.Marker([0, 0]);
marker.addTo(map);


let piechart =  createPieChart();

console.log(daysLeftToUserBirthday(users[0]));

addUsersToGrid(users);
addUsersToGallery();
//лістенер для сортування
// document.getElementById("teacher-list-th-row").addEventListener('click', sortTableHandler);
//лістенер для натиску на елементи сітки
document.getElementById("teachers-grid").addEventListener('click', gridClickHandler);
//лістенер для натиску на елементи галереї
document.getElementById("favorites-gallery-elements").addEventListener('click', gridClickHandler);
//лістенер для лівої стрілки галереї
document.getElementById("left-arrow").addEventListener('click', galleryArrowClickHandler);
//лістенер для правої стрілки галереї
document.getElementById("right-arrow").addEventListener('click', galleryArrowClickHandler);
//лістенер для кнопки пошуку
document.getElementById("header-search-button").addEventListener('click', searchHandler);
//лістенер для кнопок фільтрації юзерів
document.getElementById("top-teachers-nav").addEventListener('change', ()=>{gridPageNum = 0; currentUsers = filterUserArrayByFilters(users); addUsersToGrid(currentUsers)});
//лістенер для кнопки закриття інфо попапу
document.getElementById("info-popup-close-button").addEventListener('click', hideInfoOfUser);
//лістенер для кнопок перемикання між сторінками таблиці статистики
// document.getElementById("teacher-list-pages-refs").addEventListener('click', changePageHandler);
//лістенер для кнопки закриття попапа додавання вчителя
document.getElementById("add-popup-close-button").addEventListener('click', hideAddPopup);
//лістенер для кнопки додання юзера у формі
document.getElementById('add-form').addEventListener('submit', addTeacherFromForm);
// changePageOfTable(1);
//лістенер для кнопок додавання юзера
document.querySelectorAll('.add-teacher-button').forEach(button=>button.addEventListener('click', showAddPopup));

//лістенер для кнопки "Далі"
document.getElementById("next-page-button").addEventListener('click', switchPageOfGrid);

//won't be used anymore
// export function addUsersFromFile(){
//     let coursesArr = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry",
//         "Law", "Art", "Medicine", "Statistics"];
//     let retArr = [];
//     for(let i = 0; i < randomUserMock.length; i++){
//         let user = randomUserMock[i];
//         retArr[i] = {
//             gender: user.gender,
//             title: user.name.title,
//             full_name: user.name.first + " " + user.name.last,
//             city: user.location.city,
//             state: user.location.state,
//             country: user.location.country,
//             postcode: user.location.postcode,
//             coordinates: user.location.coordinates,
//             timezone: user.location.timezone,
//             email: user.email,
//             b_day: user.dob.date,
//             age: user.dob.age,
//             phone: user.phone,
//             picture_Large: user.picture.large,
//             picture_thumbnail: user.picture.thumbnail,
//             id:user.id.name+user.id.value,
//             favorite:false,
//             course:coursesArr[Math.floor(Math.random()*coursesArr.length)],
//             bg_color:"#FFFFFF",
//             note:"",
//         };
//     }
//     for(let i = 0; i < additionalUsers.length; i++){
//         let isPresent = false;
//         for(let j = 0; j < retArr.length; j++){
//             if(additionalUsers[i].full_name===retArr[j].full_name) {
//                 isPresent = true;
//                 break;
//             }
//         }
//         if(!isPresent) {
//             retArr.push(additionalUsers[i]);
//         }
//     }
//     return retArr;
// }

export function validateUser(user){
    user.gender = _.startCase(user.gender.toLowerCase());
    user.full_name = _.startCase(user.full_name);
    user.state = _.startCase(user.state);
    user.city = _.startCase(user.city);
    user.country = _.startCase(user.country);

    function firstIsUppercase(str, invalidIfNull) {
        if(invalidIfNull&&str === null) return false;
        if(!invalidIfNull&&(str === null||typeof str === 'undefined'||str === '')) return true;
        if (typeof str !== 'string' || (str.length === 0)) {
            return false;
        }
        if (str[0].toUpperCase() !== str[0]) {
            return false;
        }
        return true;
    }
    if(!firstIsUppercase(user.full_name, true)||user.full_name.split(" ").length!==2) {console.log("name is invalid"); return false;}
    if(!firstIsUppercase(user.gender, true)) {console.log("gender is invalid"); return false;}
    if(!firstIsUppercase(user.note, false)) {console.log("note is invalid"); return false;}
    if(!firstIsUppercase(user.state, false)) {console.log("state is invalid"); return false;}
    if(!firstIsUppercase(user.city, true)) {console.log("city is invalid"); return false;}
    if(!firstIsUppercase(user.country, true)) {console.log("country is invalid"); return false;}
    if(typeof user.age!=="number") return false;
    let phoneRegExp = new RegExp(/^[+]*[(]?[0-9]{1,3}[)]?[-\s\./0-9]*$/g);
    if(!phoneRegExp.test(user.phone)) {
        console.log("phone is invalid");
        return false;
    }
    let emailRegExp = new RegExp(/^\S+@\S+\.\S+$/);
    if(!emailRegExp.test(user.email)) {
        console.log("email is invalid");
        return false;
    }
    return true;
}

function validateUsersFromArray(userArray){
    return _.every(userArray, validateUser);
}

//фільтрація юзерів за заданими параметрами
export function filterUsersByCertainParameters(arr, country, ageLo, ageHi, gender, onlyFavorites, onlyWithPhoto) {
    function isAppropriate(user){
        if(country!=null&&user.country.toLowerCase()!==country.toLowerCase()) return false;
        if(ageLo!=null&&user.age<ageLo) return false;
        if(ageHi!=null&&user.age>ageHi) return false;
        if(gender!=null&&user.gender.toLowerCase() !== gender.toLowerCase()) return false;
        if(onlyFavorites!=null&&onlyFavorites&&!user.favorite) return false;
        if(onlyWithPhoto&&(typeof user.picture_Large == 'undefined'||user.picture_Large === '')) return false;
        return true;
    }
    return _.filter(arr, isAppropriate);
}

export function sortArrayOfUsers(userArray, fieldToSort, reverse){
    let isReverse = reverse ? -1 : 1;
    return _.orderBy(userArray, [function (user){return user[fieldToSort];}], [(reverse?'desc':'asc')]);
}


//фільтрація юзерів за результатами пошуку
export function filterUsersBySearchParameters(userArray){
    let searchValue = document.getElementById("header-search-bar").value;
    document.getElementById("header-search-bar").value = '';
    if(searchValue!==""){
        function isAppropriate(user){
                if(user.age == searchValue
                    || (typeof user.full_name != 'undefined'&&user.full_name != null&&user.full_name.toLowerCase().includes(searchValue.toLowerCase()))
                    || (typeof user.note != 'undefined'&&user.note != null&&user.note.toLowerCase().includes(searchValue.toLowerCase()))){
                    return true;
                }
                return false;
        }
        return _.filter(userArray, isAppropriate);
    }else{
        return userArray;
    }
}

export function showPercentOfAppropriate(userArray, country, ageLo, ageHi, gender, favorite, onlyWithPhoto){
    let filteredArrayLength = filterUsersByCertainParameters(userArray, country, ageLo, ageHi, gender, favorite, onlyWithPhoto).length;
    return filteredArrayLength / userArray.length * 100;
}

//функція що перетворює юзера з масиву на HTML прев'ю у вигляді стрічки
function userToUserGridElement(user) {
    let userGridString = "<div class=\"teacher-preview teachers-grid-element\" id=\"teacher-grid-person-"+ user.full_name.replaceAll(" ","_").toLowerCase()+"\">";
    userGridString += "<div class=\"teacher-image-container\">";
    let imgRef = "";
    if(typeof user.picture_Large != "undefined") imgRef = user.picture_Large;
    userGridString += "<img src=\"" + imgRef + "\" alt=\"";
    userGridString += user.full_name.split(" ")[0][0].toUpperCase();
    userGridString += ".";
    userGridString += user.full_name.split(" ")[1][0].toUpperCase()+"\"></div>";
    userGridString += "<div class=\"teacher-preview-name\"><p>";
    userGridString += user.full_name.split(" ")[0]+"</p><p>";
    userGridString += user.full_name.split(" ")[1]+"</p></div>";
    userGridString += "<div class=\"teacher-preview-speciality\"><p>";
    userGridString += user.course;
    userGridString += "</p></div><div class=\"teacher-preview-country\"><p>";
    userGridString += user.country + "</p></div><div class=\"star\"><img src=";
    if(user.favorite){
        userGridString +="\"images/star.png\"></div>";
    }else{
        userGridString +="\"images/star_empty.png\"></div></div>";
    }
    return userGridString;
}

//функція що додає юзерів з масиву до сітки
function addUsersToGrid(userArray){
    //clear whole grid
    let e = document.getElementById("teachers-grid");
    let child = e.lastElementChild;
    while (child) {
        e.removeChild(child);
        child = e.lastElementChild;
    }
    for(let i = 0; i<10&&i < userArray.length; i++){
        e.innerHTML+=userToUserGridElement(userArray[(gridPageNum*10+i)%userArray.length]);
    }
    // userArray.forEach(user => {
    //     e.innerHTML+=userToUserGridElement(user);
    // });
}

//функція що повертає юзерів з масиву за фільтрами
function filterUserArrayByFilters(userArray){
    let ageSelector = document.getElementById("top-teachers-nav-age");
    let ageLo = ageSelector.options[ageSelector.selectedIndex].value.split("-")[0];
    let ageHi = ageSelector.options[ageSelector.selectedIndex].value.split("-")[1];
    let region = document.getElementById("top-teachers-nav-region")
        .options[document.getElementById("top-teachers-nav-region").selectedIndex].value;
    if(region === "All"){ region = null;}
    let sex = document.getElementById("top-teachers-nav-sex")
        .options[document.getElementById("top-teachers-nav-sex").selectedIndex].value;
    if(sex === "All") sex = null;
    let onlyWithPhoto = document.getElementById("top-teachers-nav-only-with-photo").checked;
    let onlyFavourite = document.getElementById("top-teachers-nav-only-favourites").checked;
    console.log(ageLo);
    console.log(ageHi);
    console.log(region);
    console.log(sex);
    console.log(onlyWithPhoto);
    console.log(onlyFavourite);
    return filterUsersByCertainParameters(userArray, region, ageLo, ageHi, sex, onlyFavourite, onlyWithPhoto);
}

//додає юзерів до галереї
function addUsersToGallery(){
    let gallery = document.getElementById("favorites-gallery-elements");
    let child = gallery.lastElementChild;
    while (child) {
        gallery.removeChild(child);
        child = gallery.lastElementChild;
    }
    let favUsers = filterUsersByCertainParameters(users, null, null, null, null, true, null);
    for(let i = 0; i < favUsers.length&&i<5; i++){
        gallery.innerHTML+=userToUserGridElement(favUsers[(i+numInGallery)%favUsers.length]);
    }
}

// //заповнюємо таблицю статистики
// function addUsersToStatisticsTable(userArray){
//     let tableRows = document.getElementById("teacher-list-rows");
//
//     let child = tableRows.lastElementChild;
//     while(child&&child.querySelector("#teacher-list-th-row")==null){
//         tableRows.removeChild(child);
//         child = tableRows.lastElementChild;
//     }
//
//     for(let i = 0; i < 10; i++){
//         if(i+(statisticsPageNum-1)*10<userArray.length){
//             let user =userArray[i+(statisticsPageNum-1)*10];
//             tableRows.innerHTML+="<tr id='statistics-header-line'><td id='statistics-header-line-full-name'>"
//                 + user.full_name
//                 + "</td><td id='statistics-header-line-course'>"
//                 + user.course
//                 + "</td><td id='statistics-header-line-age'>"
//                 + user.age
//                 + "</td><td id='statistics-header-line-gender'>"
//                 + user.gender
//                 + "</td><td id='statistics-header-line-country'>"
//                 + user.country
//                 + "</td></tr>";
//         }
//     }
//     document.getElementById("teacher-list-th-row").addEventListener('click', sortTableHandler);
// }

//показує інфо-попап юзера
function showInfoOfUser(user){
    let info_popup = document.getElementById("info-popup-block");
    let imgRef = "";
    if(typeof user.picture_Large != "undefined") imgRef = user.picture_Large;
    document.getElementById("info-popup-image").firstElementChild.setAttribute("src", imgRef);
    document.getElementById("info-popup-name").innerText = user.full_name;
    document.getElementById("info-popup-subject").innerText = user.course;
    let daysToBirthday = daysLeftToUserBirthday(user);
    document.getElementById("info-popup-age-gender").innerText = user.age +", "+user.gender + ". " + (daysToBirthday!==0?"Days left to birthday: " + daysToBirthday: "Happy Birthday!");
    document.getElementById("info-popup-href").setAttribute("src","mailto: "+user.email);
    document.getElementById("info-popup-href").innerText = user.email;
    document.getElementById("info-popup-note").innerText=user.note;
    if(user.bg_color!==null) document.getElementById("info-popup-main").style.backgroundColor=user.bg_color;

    marker.removeFrom(map);
    if(typeof user.coordinates!='undefined'){
        map.setView([user.coordinates.latitude,user.coordinates.longitude],3);
        marker = new L.Marker([user.coordinates.latitude, user.coordinates.longitude]);
        marker.addTo(map);
    }
    info_popup.style.visibility="visible";
}

//ховає інфо попап
function hideInfoOfUser(){
    document.getElementById("info-popup-block").style.visibility="hidden";
}

//метод для знаходження юзера за id його елементу сітки
function findUserFromArrayByID(idInGrid){
    let userToReturn;
    users.forEach(user => {if(user.full_name.replaceAll(" ","_").toLowerCase()===idInGrid.split("-")[3]) {
        userToReturn = user;
    }});
    return userToReturn;
}

//метод для знаходження елемента сітки юзера за його js представленням
function findUserElement(user){
    return document.getElementById("teacher-grid-person-"+user.full_name.replaceAll(" ", "_").toLowerCase());
}

//метод для перехоплення кліку на елемент таблиці юзерів або галереї юзерів
function gridClickHandler(event){
    if(event.target.closest(".teachers-grid-element")!=null||event.target.closest(".favorites-gallery-elements")!=null){
        if(event.target.closest(".star")==null){
        showInfoOfUser(findUserFromArrayByID(event.target.closest(".teachers-grid-element").id));
        }
        else{
            let user = findUserFromArrayByID(event.target.closest(".teachers-grid-element").id);
            user.favorite?user.favorite=false:user.favorite=true;
            if(user.favorite){
                event.target.closest(".star").firstElementChild.setAttribute("src","images/star.png");
            }else{
                event.target.closest(".star").firstElementChild.setAttribute("src","images/star_empty.png");
            }
            addUsersToGrid(currentUsers);
            addUsersToGallery();
        }
    }
}

function galleryArrowClickHandler(event){
    if(event.target.closest("#left-arrow")!=null){
        numInGallery-=5;
        if(numInGallery<0)numInGallery+=filterUsersByCertainParameters(users, null, null, null, null, true, null).length;
    }else{
        numInGallery+=5;
    }
    addUsersToGallery();
}

//подія на натиск кнопки сортування
// function sortTableHandler(event){
//     console.log(event.target);
//     if(sortingType === event.target.id.split("-")[3]){
//         reverseSort=(reverseSort+1)%2;
//     }
//     sortingType = event.target.id.split("-")[3];
//     addUsersToStatisticsTable(sortArrayOfUsers(JSON.parse(JSON.stringify(users)), sortingType, reverseSort));
//     document.getElementById("teacher-list-th-row").addEventListener('click', sortTableHandler);
// }

// function changePageHandler(event){
//     if(typeof event.target.id.split("-")[2]!="undefined") {
//         changePageOfTable(event.target.id.split("-")[2]);
//     }
// }

// export function changePageOfTable(pageNum){
//     function pageNumToHref(text, disabled){
//         if(disabled) return "<a href=\"#\" class='disabled-link' onclick=\"return false;\">"+ text +"</a>";
//         return "<a href=\"#\" id='page-changer-"+text+"'  onclick=\"return false;\">"+text+"</a>";
//     }
//     let newPageCountersHTML = '<p>';
//     // if(pageNum!==statisticsPageNum){
//         statisticsPageNum = pageNum;
//         let leftPoints = false;
//         let rightPoints = false;
//         for(let currPage = 1; currPage <= Math.floor((users.length/10) + 1); currPage++){
//             if(currPage === statisticsPageNum){
//                 newPageCountersHTML += pageNumToHref(currPage, true);
//             }
//             else if(currPage === 1 || currPage === Math.floor((users.length/10) + 1)){
//                 newPageCountersHTML += pageNumToHref(currPage, false);
//             }
//             else if(statisticsPageNum - currPage > 2){
//                 if(!leftPoints){
//                     leftPoints = true;
//                     newPageCountersHTML += pageNumToHref("...", true);
//                 }
//             }
//             else if(currPage - statisticsPageNum > 2){
//                 if(!rightPoints){
//                     rightPoints = true;
//                     newPageCountersHTML += pageNumToHref("...", true);
//                 }
//             }
//             else{
//                 newPageCountersHTML += pageNumToHref(currPage, false);
//             }
//         }
//         newPageCountersHTML += '</p>';
//         document.getElementById('teacher-list-pages-refs').innerHTML=newPageCountersHTML;
//         addUsersToStatisticsTable(sortArrayOfUsers(users, sortingType, reverseSort));
//     // }
// }

//подія на натиск на кнопку пошуку
function searchHandler(){
    console.log("search button is pressed");
    currentUsers = filterUsersBySearchParameters(filterUsersBySearchParameters(users));
    addUsersToGrid(currentUsers);
}

function showAddPopup(){
    document.getElementById('add-popup-block').style.visibility='visible';
}
//закрити попап додання юзера
function hideAddPopup(){
    document.getElementById('add-popup-block').style.visibility='hidden';
}

function addTeacherFromForm(event){
    event.preventDefault();
        function getAge(dateString) {
            let today = new Date();
            let birthDate = new Date(dateString);
            let age = today.getFullYear() - birthDate.getFullYear();
            let m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }
    let user = {};
    user.full_name = document.getElementById('add-form-name').querySelector('input').value;
    user.b_day = document.getElementById('date').value;
    user.age = getAge(user.b_day);
    user.course = document.getElementById('add-form-speciality').querySelector('select').value;
    user.country = document.getElementById('add-form-country').querySelector('select').value;
    user.city = document.getElementById('add-form-city').querySelector('input').value;
    user.email = document.getElementById('add-form-email').querySelector('input').value;
    user.phone = document.getElementById('add-form-phone').querySelector('input').value;
    user.gender = document.getElementById('male').checked?'Male':'Female';
    user.bg_color = document.getElementById('color').value;
    user.note = document.getElementById('add-form-notes').querySelector('textarea').value;

    hideAddPopup();
    if(!validateUser(user)){
        alert('user is invalid');
    }else{
        users.push(user);
        saveUserToDB(user);
        gridPageNum = 0;
        currentUsers = users;
        addUsersToGrid(filterUserArrayByFilters(users));
        changePieChart();
    }
}

async function getRandomUsers(amount){
    const coursesArr = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry",
        "Law", "Art", "Medicine", "Statistics"];
    let response = await fetch("https://randomuser.me/api/?results="+amount);
    let data = await response.json();
    let retArray = [];
    data["results"].forEach(user =>{
       retArray.push({
               gender: user.gender,
               title: user.name.title,
               full_name: user.name.first + " " + user.name.last,
               city: user.location.city,
               state: user.location.state,
               country: user.location.country,
               postcode: user.location.postcode,
               coordinates: user.location.coordinates,
               timezone: user.location.timezone,
               email: user.email,
               b_day: user.dob.date,
               age: user.dob.age,
               phone: user.phone,
               picture_Large: user.picture.large,
               picture_thumbnail: user.picture.thumbnail,
               id:user.id.name+user.id.value,
               favorite:false,
               course:coursesArr[Math.floor(Math.random()*coursesArr.length)],
               bg_color:"#FFFFFF",
               note:"",
       });
    });
    console.log(retArray);
    return retArray;
}

function switchPageOfGrid() {
    gridPageNum++;
    addUsersToGrid(currentUsers);
}

async function saveUserToDB(user){
    await fetch("http://localhost:3000/posts", {
        method: "POST",
        headers: {
            "Content-type": "application/json",
        },
        body: JSON.stringify(user)
    })
        .then(response => response.json())
        .catch(error=>console.log(error));
}

async function getUsersFromDB(){
    return fetch("http://localhost:3000/posts"
    , {
        method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        }).then(response=>response.json());
}

function createPieChart(){
    let data = {
        labels: ["18-31", "32-44", "45-60", "61+"],
        datasets: [
            {
                label: "Teacher ages",
                data: [filterUsersByCertainParameters(users, null, 18, 38, null, null, null).length,
                    filterUsersByCertainParameters(users, null, 32, 44, null, null, null).length,
                    filterUsersByCertainParameters(users, null, 45, 60, null, null, null).length,
                    filterUsersByCertainParameters(users, null, 61, null, null, null, null).length],
                backgroundColor: [
                    "#FAEBD7",
                    "#DCDCDC",
                    "#E9967A",
                    "#F5DEB3",
                    "#9ACD32"
                ],
                borderWidth: [1, 1, 1, 1]
            }
        ]
    };

    //options
    let options = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            text: "Teachers age",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }
        }
    };

    let chart1 = new Chart(document.getElementById("piechart-canvas"), {
        type: "pie",
        data: data,
        options: options
    });
    return chart1;
}

function changePieChart(){
    piechart.data.datasets[0].data =[filterUsersByCertainParameters(users, null, 18, 38, null, null, null).length,
        filterUsersByCertainParameters(users, null, 32, 44, null, null, null).length,
        filterUsersByCertainParameters(users, null, 45, 60, null, null, null).length,
        filterUsersByCertainParameters(users, null, 61, null, null, null, null).length];
    piechart.update();
}

function daysLeftToUserBirthday(user) {
    let d1 = dayjs(user.b_day);
    let d2 = dayjs().set('year', d1.get('year'));

    return Math.abs(d1.diff(d2, 'day'));
}